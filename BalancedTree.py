"""
Copyright 2023 José Eduardo Gaboardi de Carvalho

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
"""

import types


class BalancedTree:

    def __init__(self, comparator: callable):
        if comparator is None or (not isinstance(comparator, types.FunctionType)):
            raise Exception("Invalid comparison function.")
        self.__cmp: callable = comparator
        self.__n: int = 0
        """
        atributos dos nós:
            k: key
            v: value
            b: balanceamento do nó
            l: filho esquerdo
            r: filho direito
        """
        self.__tree: dict | None = None

    def add(self, key) -> bool:
        if self.contains_key(key=key):
            return True
        self.__n += 1
        self.__tree = BalancedTree.__put_rec(raiz=self.__tree, key=key, value=None, comparador=self.__cmp)
        return False

    def ceiling(self, key):
        return self.ceiling_key(key)

    def ceiling_entry(self, key) -> tuple | None:
        p: dict = self.__tree
        maior: tuple | None = None
        while True:
            if p is None:
                return maior
            cmp = self.__cmp(key, p['k'])
            if cmp == 0:
                return p['k'], p['v']
            if cmp < 0:
                maior = (p['k'], p['v'])
                p = p['l']
            else:
                p = p['r']

    def ceiling_key(self, key):
        c = self.ceiling_entry(key)
        if c is None:
            return None
        return c[0]

    def ceiling_value(self, key):
        c = self.ceiling_entry(key)
        if c is None:
            return None
        return c[1]

    def clear(self):
        self.__n = 0
        self.__tree = None

    @property
    def comparator(self) -> types.FunctionType:
        return self.__cmp

    @staticmethod
    def __confere_arvore(raiz: dict, comparador: callable) -> (int, bool):
        # devolve a altura e se a árvore com esta raiz está ok, para conferência da árvore.
        if raiz is None:
            return -1, True
        a1: int
        a2: int
        a1, ok = BalancedTree.__confere_arvore(raiz['l'], comparador)
        if not ok:
            return 0, False
        a2, ok = BalancedTree.__confere_arvore(raiz['r'], comparador)
        if not ok:
            return 0, False
        if a1 > a2 + 1:
            return 0, False
        if a2 > a1 + 1:
            return 0, False
        # Conferir balanceamento.
        if a1 == a2:
            if raiz['b'] != 0:
                return 0, False
        elif a1 > a2:
            if raiz['b'] != -1:
                return 0, False
        elif a1 < a2:
            if raiz['b'] != 1:
                return 0, False
        # Conferir diferença de altura.
        if a1 > a2:
            a1 += 1
        else:
            a1 = a2 + 1
        if raiz['l'] is not None:
            if comparador(raiz['l']['k'], raiz['k']) >= 0:
                return False
        if raiz['r'] is not None:
            if comparador(raiz['k'], raiz['r']['k']) >= 0:
                return False
        return a1, True

    @staticmethod
    def __conta_nos(no: dict) -> int:
        # funcao de uso interno para conferencia da arvore
        if no is None:
            return 0
        return 1 + BalancedTree.__conta_nos(no['l']) + BalancedTree.__conta_nos(no['r'])

    def contains(self, key):
        return self.contains_key(key)

    def contains_key(self, key) -> bool:
        g = self.get_entry(key)
        if g is None:
            return False
        return True

    def entry_set(self):
        # devolve uma lista ordenada com todas as entradas da árvore.
        lista: list = list()
        BalancedTree.__entry_set_rec(lista, self.__tree)
        return lista

    @staticmethod
    def __entry_set_rec(lista: list, raiz):
        if raiz is None:
            return
        BalancedTree.__entry_set_rec(lista, raiz['l'])
        lista.append((raiz['k'], raiz['v']))
        BalancedTree.__entry_set_rec(lista, raiz['r'])

    def first(self):
        return self.first_key()

    def first_entry(self) -> tuple | None:
        p: dict = self.__tree
        if self.__n == 0:
            return None
        while p['l'] is not None:
            p = p['l']
        return p['k'], p['v']

    def first_key(self):
        f = self.first_entry()
        if f is None:
            return None
        return f[0]

    def first_value(self):
        f = self.first_entry()
        if f is None:
            return None
        return f[1]

    def floor(self, key):
        return self.floor_key(key)

    def floor_entry(self, key) -> tuple | None:
        p: dict = self.__tree
        menor: tuple | None = None
        while True:
            if p is None:
                return menor
            cmp = self.__cmp(key, p['k'])
            if cmp == 0:
                return p['k'], p['v']
            if cmp > 0:
                menor = (p['k'], p['v'])
                p = p['r']
            else:
                p = p['l']

    def floor_key(self, key):
        f = self.floor_entry(key)
        if f is None:
            return None
        return f[0]

    def floor_value(self, key):
        f = self.floor_entry(key)
        if f is None:
            return None
        return f[1]

    def get(self, key):
        g = self.get_entry(key)
        if g is None:
            return None
        return g[1]

    def get_entry(self, key) -> tuple | None:
        cmp: int
        p: dict = self.__tree
        while p is not None:
            cmp = self.__cmp(key, p['k'])
            if cmp < 0:
                p = p['l']
            elif cmp > 0:
                p = p['r']
            else:
                return p['k'], p['v']
        return None

    def get_comparator(self) -> types.FunctionType:
        return self.__cmp

    def height(self) -> int:
        h: int = -1
        p: dict = self.__tree
        while p is not None:
            h += 1
            p = p['l'] if p['b'] <= 0 else p['r']
        return h

    def higher(self, key):
        return self.higher_key(key)

    def higher_entry(self, key) -> tuple | None:
        p: dict = self.__tree
        maior: tuple | None = None
        while True:
            if p is None:
                return maior
            cmp = self.__cmp(key, p['k'])
            if cmp < 0:
                maior = (p['k'], p['v'])
                p = p['l']
            else:
                p = p['r']

    def higher_key(self, key):
        h = self.higher_entry(key)
        if h is None:
            return None
        return h[0]

    def higher_value(self, key):
        h = self.higher_entry(key)
        if h is None:
            return None
        return h[1]

    def is_empty(self) -> bool:
        if self.__n == 0:
            return True
        return False

    def key_set(self) -> list:
        # Devolve uma lista ordenada com todas as chaves da árvore.
        lista: list = list()
        BalancedTree.__key_set_rec(lista, self.__tree)
        return lista

    @staticmethod
    def __key_set_rec(lista: list, raiz):
        if raiz is None:
            return
        BalancedTree.__entry_set_rec(lista, raiz['l'])
        lista.append(raiz['k'])
        BalancedTree.__entry_set_rec(lista, raiz['r'])

    def last(self):
        return self.last_key()

    def last_entry(self) -> tuple | None:
        p: dict = self.__tree
        if self.__n == 0:
            return None
        while p['r'] is not None:
            p = p['r']
        return p['k'], p['v']

    def last_key(self):
        la = self.last_entry()
        if la is None:
            return None
        return la[0]

    def last_value(self):
        la = self.last_entry()
        if la is None:
            return None
        return la[1]

    def lower(self, key):
        return self.lower_key(key)

    def lower_entry(self, key) -> tuple | None:
        p: dict = self.__tree
        menor: tuple | None = None
        while True:
            if p is None:
                return menor
            cmp = self.__cmp(key, p['k'])
            if cmp > 0:
                menor = (p['k'], p['v'])
                p = p['r']
            else:
                p = p['l']

    def lower_key(self, key):
        lo = self.lower_entry(key)
        if lo is None:
            return None
        return lo[0]

    def lower_value(self, key):
        lo = self.lower_entry(key)
        if lo is None:
            return None
        return lo[1]

    @property
    def n(self) -> int:
        return self.__n

    def pool_first(self):
        p = self.pool_first_entry()
        if p is None:
            return None
        return p[0]

    def pool_first_entry(self) -> tuple | None:
        f = self.first_entry()
        if f is None:
            return None
        self.remove(f[0])
        return f

    def pool_last(self):
        p = self.pool_last_entry()
        if p is None:
            return None
        return p[0]

    def pool_last_entry(self) -> tuple | None:
        la = self.last_entry()
        if la is None:
            return None
        self.remove(la[0])
        return la

    def put(self, key, value):
        # Devolve o valor anterior, se já existia. None se não existia.
        valor_anterior = None
        g = self.get_entry(key)
        if g is not None:
            valor_anterior = g[1]
        else:
            self.__n += 1
        self.__tree = BalancedTree.__put_rec(raiz=self.__tree, key=key, value=value, comparador=self.__cmp)
        return valor_anterior

    @staticmethod
    def __put_rec(raiz: dict, key, value, comparador: callable):
        if raiz is None:
            return {'k': key, 'v': value, 'b': 0, 'l': None, 'r': None}
        cmp: int
        cmp = comparador(key, raiz['k'])
        if cmp == 0:  # já existia a chave, apenas substitui o valor e devolve o valor anterior
            raiz['v'] = value
            return raiz
        bal_filho_antes: int
        bal_filho_depois: int
        if cmp < 0:  # inserir na subárvore esquerda
            if raiz['l'] is None:
                raiz['l'] = {'k': key, 'v': value, 'b': 0, 'l': None, 'r': None}
                if raiz['r'] is None:
                    raiz['b'] = -1
                else:
                    raiz['b'] = 0
                return raiz
            bal_filho_antes = raiz['l']['b']
            raiz['l'] = BalancedTree.__put_rec(raiz['l'], key, value, comparador)
            bal_filho_depois = raiz['l']['b']
            if bal_filho_depois == 0 or bal_filho_depois == bal_filho_antes:
                # não mudou altura do filho, com ou sem rebalanceamento feito
                return raiz
            if raiz['b'] != -1:  # filho esquerdo aumentou
                raiz['b'] -= 1
                return raiz
            if bal_filho_depois < 0:
                # desbalanceou a raiz com excesso de altura à esquerda
                return BalancedTree.__rot_simples_direita(raiz)
            else:
                # desbalanceou a raiz com excesso de altura à esquerda e internamente (direta do filho esquerdo)
                return BalancedTree.__rot_dupla_esquerda_direita(raiz)
        else:  # inserir na subárvore direita
            if raiz['r'] is None:
                raiz['r'] = {'k': key, 'v': value, 'b': 0, 'l': None, 'r': None}
                if raiz['l'] is None:
                    raiz['b'] = 1
                else:
                    raiz['b'] = 0
                return raiz
            bal_filho_antes = raiz['r']['b']
            raiz['r'] = BalancedTree.__put_rec(raiz['r'], key, value, comparador)
            bal_filho_depois = raiz['r']['b']
            if bal_filho_depois == 0 or bal_filho_depois == bal_filho_antes:
                # não mudou altura do filho, com ou sem rebalanceamento feito
                return raiz
            if raiz['b'] != 1:  # filho direito aumentou
                raiz['b'] += 1
                return raiz
            if bal_filho_depois > 0:
                # desbalanceou a raiz com excesso de altura à direita
                return BalancedTree.__rot_simples_esquerda(raiz)
            else:
                # desbalanceou a raiz com excesso de altura à direita e internamente (esquerda do filho direito)
                return BalancedTree.__rot_dupla_direita_esquerda(raiz)

    def remove(self, key):
        if not self.contains_key(key):
            return False
        self.remove_entry(key)
        return True

    def remove_entry(self, key):
        eliminado = self.get_entry(key)
        if eliminado is None:  # elemento não existe na árvore
            return None

        if self.__n == 1:  # Árvore ficará vazia.
            self.__n = 0
            self.__tree = None
            return eliminado[1]

        # Achar elemento para ver se já é nó folha.
        cmp: int
        comparador: callable = self.__cmp
        p: dict
        p2: dict
        p = self.__tree
        while True:
            if p is None:
                raise Exception("remove_entry: key not found.")
            cmp = comparador(key, p['k'])
            if cmp == 0:
                break
            if cmp < 0:
                p = p['l']
            else:
                p = p['r']

        if p['l'] is None and p['r'] is None:  # já é nó folha, remover diretamente pela rotina recursiva que balanceia.
            self.__n -= 1
            self.__tree = BalancedTree.__remove_folha_rec(self.__tree, key, comparador)
            return eliminado[1]

        # Procurar substituto para exclusão no sucessor (ou antecessor) que estiver na subárvore.
        substituto: dict
        if p['r'] is not None:
            substituto = p['r']
            while substituto['l'] is not None:
                substituto = substituto['l']
        else:
            substituto = p['l']
            while substituto['r'] is not None:
                substituto = substituto['r']
        substituto_key = substituto['k']
        substituto_value = substituto['v']

        # Eliminar recursivamente, e substituir o elemento a ser eliminado pelo substituto que foi fisicamente
        # eliminado. Eventualmente vai achar uma folha que será facilmente eliminada, voltando as recursões.
        self.remove(substituto_key)

        # Achar novamente o elemento para substituir pelo substituto que foi fisicamente eliminado.
        p = self.__tree
        while True:
            if p is None:
                raise Exception("remove_entry: key not found (second search).")
            cmp = comparador(key, p['k'])
            if cmp == 0:
                break
            if cmp < 0:
                p = p['l']
            else:
                p = p['r']
        p['k'] = substituto_key
        p['v'] = substituto_value
        return eliminado[1]

    @classmethod
    def __remove_folha_rec(cls, raiz: dict, key, comparador) -> dict | None:
        # Aqui deverá ser garantido que o valor a ser eliminado é de um nó folha.
        if raiz is None:
            raise Exception("__remove_folha_rec: 'None' root.")
        bal_filho_antes: int
        bal_filho_depois: int
        cmp: int
        cmp = comparador(key, raiz['k'])
        if cmp == 0:
            if raiz['l'] is not None or raiz['r'] is not None:
                raise Exception("__remove_folha_rec com remocao de chave de no' nao folha.")
            raiz['k'] = None
            raiz['v'] = None
            raiz['b'] = None
            return None
        if cmp > 0:  # excluir da direita
            bal_filho_antes = raiz['r']['b']
            raiz['r'] = BalancedTree.__remove_folha_rec(raiz=raiz['r'], key=key, comparador=comparador)

            # Casos de balanceamento

            # Tratar os casos de filhos vazios antes para evitar referenciar balancamentos inexistentes.
            if raiz['l'] is None:  # filho esquerdo vazio
                if raiz['r'] is None:
                    raiz['b'] = 0
                    return raiz
                raise Exception("__remove_folha_rec: invalid AVL tree.")
            elif raiz['r'] is None:  # filho direito ficou vazio
                if raiz['l']['l'] is None and raiz['l']['r'] is None:
                    raiz['b'] = -1
                    return raiz
                if raiz['l']['b'] <= 0:  # rotacao simples direita
                    return BalancedTree.__rot_simples_direita(raiz)
                else:  # rotacao dupla esquerda direita
                    return BalancedTree.__rot_dupla_esquerda_direita(raiz)

            # neste pontos tem ambos os filhos preenchidos
            bal_filho_depois = raiz['r']['b']
            if bal_filho_antes == bal_filho_depois or bal_filho_depois != 0:
                return raiz

            # aqui diminuiu altura do filho direito
            if raiz['b'] != -1:
                raiz['b'] -= 1
                return raiz
            # aqui raiz desbalanceou, pois já pendia para esquerda.
            if raiz['l']['b'] <= 0:  # rotacao simples direita
                return BalancedTree.__rot_simples_direita(raiz)
            else:  # rotacao dupla esquerda direita
                return BalancedTree.__rot_dupla_esquerda_direita(raiz)
        else:  # excluir da esquerda
            bal_filho_antes = raiz['l']['b']
            raiz['l'] = BalancedTree.__remove_folha_rec(raiz=raiz['l'], key=key, comparador=comparador)

            # Casos de balanceamento

            # Tratar os casos de filhos vazios antes para evitar referenciar balancamentos inexistentes.
            if raiz['r'] is None:  # filho direito vazio
                if raiz['l'] is None:
                    raiz['b'] = 0
                    return raiz
                raise Exception("__remove_folha_rec: invalid AVL tree.")
            elif raiz['l'] is None:  # filho esquerdo ficou vazio
                if raiz['r']['l'] is None and raiz['r']['r'] is None:
                    raiz['b'] = 1
                    return raiz
                if raiz['r']['b'] >= 0:  # rotacao simples esquerda
                    return BalancedTree.__rot_simples_esquerda(raiz)
                else:  # rotacao dupla direita esquerda
                    return BalancedTree.__rot_dupla_direita_esquerda(raiz)

            # neste pontos tem ambos os filhos preenchidos
            bal_filho_depois = raiz['l']['b']
            if bal_filho_antes == bal_filho_depois or bal_filho_depois != 0:
                return raiz

            # aqui diminuiu altura do filho esquerdo
            if raiz['b'] != 1:
                raiz['b'] += 1
                return raiz
            # aqui raiz desbalanceou, pois já pendia para direita.
            if raiz['r']['b'] >= 0:  # rotacao simples esquerda
                return BalancedTree.__rot_simples_esquerda(raiz)
            else:  # rotacao dupla direita esquerda
                return BalancedTree.__rot_dupla_direita_esquerda(raiz)

    @staticmethod
    def __rot_dupla_direita_esquerda(raiz: dict) -> dict:
        # conj_a: dict = raiz['r']['r']
        no_b: dict = raiz['r']
        conj_c: dict = raiz['r']['l']['r']
        no_d: dict = raiz['r']['l']
        conj_e: dict = raiz['r']['l']['l']
        no_f: dict = raiz
        # conj_g: dict = raiz['l']['l']
        no_b['l'] = conj_c
        no_d['r'] = no_b
        no_d['l'] = no_f
        no_f['r'] = conj_e
        if no_d['b'] == 0:
            no_b['b'] = 0
            no_f['b'] = 0
        elif no_d['b'] == 1:
            no_b['b'] = 0
            no_f['b'] = -1
        else:
            no_b['b'] = 1
            no_f['b'] = 0
        no_d['b'] = 0
        return no_d

    @staticmethod
    def __rot_dupla_esquerda_direita(raiz: dict) -> dict:
        # conj_a: dict = raiz['l']['l']
        no_b: dict = raiz['l']
        conj_c: dict = raiz['l']['r']['l']
        no_d: dict = raiz['l']['r']
        conj_e: dict = raiz['l']['r']['r']
        no_f: dict = raiz
        # conj_g: dict = raiz['r']['r']
        no_b['r'] = conj_c
        no_d['l'] = no_b
        no_d['r'] = no_f
        no_f['l'] = conj_e
        if no_d['b'] == 0:
            no_b['b'] = 0
            no_f['b'] = 0
        elif no_d['b'] == -1:
            no_b['b'] = 0
            no_f['b'] = 1
        else:
            no_b['b'] = -1
            no_f['b'] = 0
        no_d['b'] = 0
        return no_d

    @staticmethod
    def __rot_simples_direita(raiz: dict) -> dict:
        # conj_a = raiz['l']['l']
        no_b: dict = raiz['l']
        conj_c: dict = raiz['l']['r']
        no_d: dict = raiz
        # conj_e = raiz['r']
        no_d['l'] = conj_c
        no_b['r'] = no_d
        if no_b['b'] == 0:
            no_b['b'] = 1
            no_d['b'] = -1
        else:
            no_b['b'] = 0
            no_d['b'] = 0
        return no_b

    @staticmethod
    def __rot_simples_esquerda(raiz: dict) -> dict:
        # conj_a = raiz['r']['r']
        no_b: dict = raiz['r']
        conj_c: dict = raiz['r']['l']
        no_d: dict = raiz
        # conj_e = raiz['l']
        no_d['r'] = conj_c
        no_b['l'] = no_d
        if no_b['b'] == 0:
            no_b['b'] = -1
            no_d['b'] = 1
        else:
            no_b['b'] = 0
            no_d['b'] = 0
        return no_b

    def size(self) -> int:
        return self.__n

    def valid_avl_tree(self) -> bool:
        if self.__n == 0:
            if self.__tree is None:
                return True
            return False
        if self.__tree is None and self.__n != 0:
            return False
        n: int
        altura: int
        n = BalancedTree.__conta_nos(self.__tree)
        if n != self.__n:
            return False
        _, ok = BalancedTree.__confere_arvore(self.__tree, self.__cmp)
        if not ok:
            return False
        return True
