from BalancedTree import BalancedTree
import random


def comparador(a: int, b: int) -> int:
    if a < b:
        return -1
    if a > b:
        return 1
    return 0


def main():
    avl = BalancedTree(comparator=comparador)
    for i in range(1000000):
        avl.add(i)
    print(avl.n)
    print(avl.height())
    print(f"Valida? {avl.valid_avl_tree()}")
    rnd = random.Random()
    seed = int(random.random() * 1000)
    print(f"Usando seed {seed}")
    rnd.seed(seed)
    for i in range(1000000):
        # if i == 278:
        #     print("Vai excluir 278?")
        if rnd.random() < .5:
            """
            if i == 278:
                print("Sim")
                print("Arvore está valida neste ponto?")
                print(avl.valid_avl_tree())
            """
            avl.remove(i)
            """
            if not avl.valid_avl_tree():
                print(f"Arvore deixou de ser valida apos exclusao de {i}.")
                raise Exception("Arvore ficou invalida.")
            """
        """
        else:
            if i == 278:
                print("Nao")
        """
    print(avl.n)
    print(avl.height())
    print(f"Valida? {avl.valid_avl_tree()}")


if __name__ == "__main__":
    main()
